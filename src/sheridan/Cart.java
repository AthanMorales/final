/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import java.util.ArrayList;
import java.util.List;
import sheridan.DiscountFactory.DiscountType;

/**
 *
 * @author Mauricio
 */
public class Cart {
    DiscountFactory factory = DiscountFactory.getInstance();
    Discount discountAmount;
    Discount discountPercentage;
    
    private List<Product> products;
    private PaymentService service;

    public Cart() {
        products = new ArrayList<Product>();
    }
    
    public void setPaymentService(PaymentService service) {
        this.service = service;
    }
    
    public void addProduct(Product product){
        products.add(product);
    }
    
    public void payCart(){
        double totalPrice=0;
        for(Product p: products){
           discountAmount = factory.getDiscount(DiscountType.AMOUNT,p.getPrice());
           discountPercentage = factory.getDiscount(DiscountType.PERCENTAGE,p.getPrice());
           totalPrice += p.getPrice();
        }
        service.ProcessPayment(totalPrice);
    }
    
    public int getCartSize(){
        return products.size();
    }
}
