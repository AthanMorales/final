/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Mauricio
 */
public class PaymentServiceFactory {
    private static PaymentServiceFactory payFactory;

    private PaymentServiceFactory() {
    }

    public static PaymentServiceFactory getInstance() {
        if (payFactory == null) {
            payFactory = new PaymentServiceFactory();
        }
        return payFactory;
    }
    
    public PaymentService getPaymentService(PaymentServiceType type) {
        switch(type){
            case CREDIT: return new CreditPaymentService();
            case DEBIT: return new DebitPaymentService();
        }
        return null;
    }
}
