/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Mauricio
 */
public class DiscountByAmount extends Discount{
    private double discount;
    
    public DiscountByAmount(double discount) {
        this.discount = discount;
    }
    
    @Override
    public void calculateDiscount(double amount) {
        super.amount = amount-this.discount;
    }
}
