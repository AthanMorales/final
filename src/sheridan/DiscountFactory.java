/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Mauricio
 */
public class DiscountFactory {
    public enum DiscountType {AMOUNT,PERCENTAGE};
    private static DiscountFactory discountFactory;

    private DiscountFactory() {
    }

    public static DiscountFactory getInstance() {
        if (discountFactory == null) {
            discountFactory = new DiscountFactory();
        }
        return discountFactory;
    }
    
    public Discount getDiscount(DiscountType type, double amount) {
        switch(type){
            case AMOUNT: return new DiscountByAmount(amount);
            case PERCENTAGE: return new DiscountByPercentage(amount);
        }
        return null;
    }
}
