/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Mauricio
 */
public class DiscountByPercentage extends Discount{
    private double discount;
    
    public DiscountByPercentage(double discount) {
        this.discount = discount;
    }

    @Override
    public void calculateDiscount(double amount) {
        super.amount = amount-(amount * this.discount/100);
    }
}
