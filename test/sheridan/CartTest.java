/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Athan
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddProductRegular() {
        System.out.println("addProduct Regular");
        Product product = null;
        Cart instance = new Cart();
        instance.addProduct(product);
        boolean expResult = true;
        boolean result = instance.getCartSize() > 0;
        assertEquals("The size of the lits did not change",expResult, result);
    }
    
    @Test
    public void testAddProductExeptional() {
        System.out.println("addProduct Exeptional");
        Product product = null;
        Cart instance = new Cart();
        boolean expResult = false;
        boolean result = instance.getCartSize() > 0;
        assertEquals("The size of the lits did not change",expResult, result);
    }  
}
